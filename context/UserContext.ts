import { createContext } from "react";
import { User } from "../models";

type ContextProps = {
    userData: User | null;
    setUserData: (userData: User | null) => void;
    token: string | null;
    setToken: (value: string | null) => void;
}

export const UserContext =  createContext<ContextProps>({
    userData: null,
    setUserData: () => null,
    token: null,
    setToken: () => null
});