import { setupInterceptorsTo } from "../lib/AxiosInterceptor";
import axios, { AxiosInstance } from "axios";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useContext, useEffect, useState } from "react";
import { UserContext } from "../context/UserContext";


export const useAuth = () => {

  const { token } = useContext(UserContext);
  //const [token, setToken] = useState<string | null>(null);

  const instance = axios.create({
    baseURL: 'http://192.168.100.16:8000/api/'
  });

  if(token) instance.defaults.headers.common['Authorization'] = `Bearer ${token}`

  const api = setupInterceptorsTo(instance);

  const signIn = (email: string, password: string, device_name: string | null) => { 
    return api.post("signin", {
      email,
      password,
      device_name
    });
  };

  const logOut = () => {
    return api.post("logout");
  };

  const profile = (name: string, phone: string, surname?: string) => {
    return api.put("v1/users", {
      name,
      surname,
      phone
    });
  }

  const dashboard = () => {
    return api.get('v1/classrooms/sil-a/sil-a')
  }

  const studentList = () => {
    return api.get('v1/classrooms/students')
  }

  const studentListPaginator = (page: number) => {
    return api.get('v1/classrooms/students?page='+page)
  }

  const sequenceList = () => {
    return api.get('v1/sequences')
  }

  const courseList = () => {
    return api.get('v1/courses')
  }

  return {
    signIn,
    logOut,
    profile,
    dashboard,
    courseList,
    studentList,
    sequenceList,
    studentListPaginator
  }
}

 
