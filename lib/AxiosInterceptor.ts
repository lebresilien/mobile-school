import {AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse} from "axios";
import { useContext } from "react";
import { UserContext } from "../context/UserContext";


const onRequest = (config: AxiosRequestConfig): AxiosRequestConfig => {
  console.log(`[request] [${JSON.stringify(config)}]`);
  return config;
}

const onRequestError = (error: AxiosError): Promise<AxiosError> => {
  console.error(`[request error] [${JSON.stringify(error)}]`);
  return Promise.reject(error);
}

const onResponse = (response: AxiosResponse): AxiosResponse => {
  console.info(`[response] [${JSON.stringify(response)}]`);
  return response;
}

const onResponseError = (error: AxiosError): Promise<AxiosError> => {
  //console.error(`[response error] [${JSON.stringify(error)}]`);
  if(error.response?.status == 401) {
    
  }
  return Promise.reject(error);
}

export function setupInterceptorsTo(axiosInstance: AxiosInstance): AxiosInstance {
  const { setUserData } = useContext(UserContext);
  axiosInstance.interceptors.request.use(onRequest, onRequestError);
  axiosInstance.interceptors.response.use(onResponse, (error: AxiosError): Promise<AxiosError> => {
    console.error(`[response error] [${JSON.stringify(error)}]`);
    if(error.response?.status == 401) {
      //setUserData(null)
    }
    return Promise.reject(error);
  });
  return axiosInstance;
}

