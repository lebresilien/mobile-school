export type User = {
    name: string,
    surname: string,
    email: string
    phone: string,
    rules: string[]
};


export type Student = {
    id: string,
    name: string,
    surname: string,
    matricule: string,
    sexe?: string,
    bornAt?: string,
    bornPlace?: string,
    fName?: string,
    mName?: string,
    fPhone?: string,
    mPhone?: string,
    allergy?: string,
    quarter?: string,
}

export type Sequence = {
    id: number,
    name: string,
    slug: string,
    status: boolean
}

export type Course = {
    id: number,
    name: string
}