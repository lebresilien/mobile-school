import { Center, Box, Image, Heading, Text, Flex } from "native-base";
import { SafeAreaView, StyleSheet } from 'react-native';
import HeaderNavigation from "../components/HeaderNavigation";
import InputField from "../components/InputField";
import MyButton from "../components/MyButton";
import type { StackScreenProps  } from '@react-navigation/stack';
import { RootStackParamList } from "../navigation/StackNavigator";
import { useState } from "react";

type Props = StackScreenProps<RootStackParamList, 'Forget'>;

const forgot = './../assets/forgot.png';

const ForgetScreen = ({ navigation }: Props) => {

    const navigate = () => navigation.goBack();
    const reset = () => navigation.navigate('Reset');
    const [email, setEmail] = useState('');
    const [loading, setLoading] = useState(false);

    return (
        <SafeAreaView style={styles.container}>

            <HeaderNavigation title="Mot de passe oublié" navigate={navigate} /> 
            
            <Flex px={'3'} py={10} alignItems={'center'} height={"80%"} justifyContent={'space-between'}>

                <Image 
                    source={require(forgot)}
                    alt="forgot password"
                    size={'xl'}
                />

                <Heading size="md" fontWeight="600" color="coolGray.800">
                    Mot de passe oublié?
                </Heading>

                <Text fontSize="xs" maxW={350}>
                    Pas de panique!Entrer l'adresse associée à votre compte
                    pour recevoir le lien de renitialisation.
                </Text>


                <InputField type="text" placeholder="Email" onChangeText={(value: string) => setEmail(value)} />

                <MyButton mt={2} isDisabled={loading} isLoading={loading} label={"Renitialiser"} onPress={reset} w="full" isLoadingText={"Renitialisation ..."} />

            </Flex>
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
  });

export default ForgetScreen;