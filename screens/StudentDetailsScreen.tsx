import { Box, Flex, Divider, Heading, VStack, Avatar, Text, Center, ScrollView } from "native-base"
import { SafeAreaView, StyleSheet } from "react-native";
import { RootTabParamList } from "../navigation/TabNavigator";
import { BottomTabScreenProps } from "@react-navigation/bottom-tabs";
import { useEffect } from "react";
import HeaderNavigation from "../components/HeaderNavigation";

type Props =  BottomTabScreenProps<RootTabParamList, 'StudentDetails'>;

const StudentDetailsScreen = ({ route, navigation }: Props) => {

    useEffect(() => { 
        navigation.setOptions({
            tabBarStyle: { display: 'none' },
        });
        return () => navigation.setOptions({
            tabBarStyle: { display: 'flex' },
        });
    },[navigation]);

    const navigate = () => navigation.navigate('Student');

    const { student } = route.params;

    return (
        <SafeAreaView style={styles.container}>

            <HeaderNavigation title="Details" navigate={navigate} /> 

            <ScrollView>

                <Box px={1}>
                    
                    <VStack>
                        <Center>
                            <VStack alignItems={'center'}>
                                <Avatar size="lg">
                                    { (student.surname.charAt(0)).toUpperCase() + (student.name.charAt(0)).toUpperCase()  }
                                </Avatar>
                                <Heading color="coolGray.600" fontWeight="light" size="xs">
                                    { student.name + student.surname }
                                </Heading>
                                <Heading color="coolGray.600" fontWeight="light" size="xs">
                                    { student.matricule }
                                </Heading>
                                <Heading color="coolGray.600" fontWeight="light" size="xs">
                                    { student.sexe == 'M' ? 'Masculin' : 'Feminin' }
                                </Heading>
                                <Heading color="coolGray.600" fontWeight="light" size="xs">
                                    { student.quarter }
                                </Heading>
                            </VStack>

                            <Text fontSize='xs' my={3}>
                                { student.allergy }
                            </Text>

                            <Flex direction="row" width={'90%'}>

                                <Box w={'1/2'}>
                                    <Text>
                                        { student.born_at }
                                    </Text> 
                                    <Text fontSize={'xs'}>
                                        Né le 
                                    </Text>
                                </Box>

                                <Box w={'1/2'}>
                                    <Text color="coolGray.600">
                                        { student.born_place }
                                    </Text> 
                                    <Text fontSize={'xs'}>
                                        A
                                    </Text>
                                </Box>

                            </Flex>

                            <Divider my={1} />

                            <Flex direction="row" width={'90%'}>

                                <Box w={'1/2'}>
                                    <Text>
                                        { student.father_name }
                                    </Text> 
                                    <Text fontSize={'xs'}>
                                        Nom du pére
                                    </Text>
                                </Box>

                                <Box w={'1/2'}>
                                    <Text color="coolGray.600">
                                        { student.fphone }
                                    </Text> 
                                    <Text fontSize={'xs'}>
                                        Téléphone
                                    </Text>
                                </Box>

                            </Flex>

                            <Flex direction="row" w={'90%'} my={3}>

                                <Box w={'1/2'}>
                                    <Text color="coolGray.600" fontWeight="medium" >
                                        { student.mother_name }
                                    </Text> 
                                    <Text fontSize={'xs'}>
                                        Nom de la mére
                                    </Text>
                                </Box>

                                <Box w={'1/2'}>
                                    <Text color="coolGray.600" fontWeight="medium">
                                        { student.mphone }
                                    </Text> 
                                    <Text fontSize={'xs'}>
                                        Téléphone
                                    </Text>
                                </Box>

                            </Flex>

                        </Center>

                    </VStack>
                    
                </Box>
            </ScrollView>
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
});

export default StudentDetailsScreen;