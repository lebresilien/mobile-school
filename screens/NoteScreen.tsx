import { Center, Box, ScrollView, Flex, Text } from "native-base";
import { useState, useEffect } from "react"; 
import { useAuth } from "../api/userAuth";
import Loader from '../components/Loader';
import { SafeAreaView, StyleSheet } from 'react-native';
import { RootTabParamList } from "../navigation/TabNavigator";
import { BottomTabScreenProps } from "@react-navigation/bottom-tabs";
import { Sequence } from "../models";

type Props = BottomTabScreenProps<RootTabParamList, 'Note'>;

const NoteScreen = ({ navigation }: Props) => {

    const [loading, setLoading] = useState(true);
    const [sequences, setSequences] = useState<Sequence[]>([]);
    const { sequenceList } = useAuth();

    useEffect(() => {
        function fetch() {
            sequenceList().then((res) => {
            setSequences(res.data);
            setLoading(false);
        });
        }
        fetch();
    }, []);

    return (
        <SafeAreaView style={styles.container}>
            
            { loading ?

                <Loader />:
                
                <ScrollView py={10}>
                   <Flex direction="row" justify="center" align="center">
                        {sequences?.map((sequence) => (
                            <Box key={sequence.id} rounded={'sm'} w={'1/2'} p={5} alignItems={'center'}>
                                <Text>{ sequence.name }</Text>
                            </Box>
                        ))}
                   </Flex>
                </ScrollView>
            }
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
});

export default NoteScreen;