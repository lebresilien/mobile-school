import { Icon, Pressable, Heading, Text, Flex, VStack  } from "native-base";
import { SafeAreaView, StyleSheet } from 'react-native';
import HeaderNavigation from "../components/HeaderNavigation";
import InputField from "../components/InputField";
import MyButton from "../components/MyButton";
import { useState } from "react";
import { MaterialIcons } from '@expo/vector-icons';
import type { StackScreenProps  } from '@react-navigation/stack';
import { RootStackParamList } from "../navigation/StackNavigator";

type Props = StackScreenProps<RootStackParamList, 'Reset'>;

const forgot = './../assets/forgot.png';

const ResetScreen = ({ navigation }: Props) => {

    const [showPassword, setShowPassword] = useState(false);
    const [Password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);

    const navigate = () => navigation.goBack();
    const reset = () => {}

    return (
        <SafeAreaView style={styles.container}>

            <HeaderNavigation title="Nouveau mot de passe" navigate={navigate} /> 
            
            <Flex px={'3'} py={10} alignItems={'center'} height={"90%"} justifyContent={'space-between'}>

                <Heading size="md" fontWeight="600" color="coolGray.800">
                    Créer un nouveau mot de passe
                </Heading>

                <Text fontSize="xs" maxW={350}>
                   Votre nouveau mot de passe doit etre different de l'ancien et 
                   doit contenir au moins 8 caractéres.
                </Text> 

                <VStack>
                    <InputField 
                        onChangeText={(value: string) => setPassword(value)}
                        placeholder="Mot de passe" 
                        type={showPassword ? "text" : "password"}
                        InputRightElement={<Pressable onPress={() => setShowPassword(!showPassword)}>
                            <Icon as={<MaterialIcons name={showPassword ? "visibility" : "visibility-off"} />} size={5} mr="2" color="muted.400" />
                        </Pressable>}
                    />
                    <Text fontSize="xs" color={"gray.400"}>
                        Votre mot de passe doit contenir au moins 8 caractères.
                    </Text>
                </VStack>

                <VStack>
                    <InputField 
                        onChangeText={(value: string) => setConfirmPassword(value)}
                        placeholder="Confirmer votre mot de passe" 
                        type={showConfirmPassword ? "text" : "password"}
                        InputRightElement={<Pressable onPress={() => setShowConfirmPassword(!showConfirmPassword)}>
                            <Icon as={<MaterialIcons name={showConfirmPassword ? "visibility" : "visibility-off"} />} size={5} mr="2" color="muted.400" />
                        </Pressable>}
                    />
                    <Text fontSize="xs" color={"gray.400"}>
                        Les mots de passe doivent êtres identiques.
                    </Text>
                </VStack>

                <MyButton mt={2} isDisabled={loading} isLoading={loading} label={"Renitialiser"} isLoadingText={"Renitialisation ..."} onPress={reset} w="full" />

            </Flex>
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
  });

export default ResetScreen;