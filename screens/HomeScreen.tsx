import { Center, Box, ScrollView, HStack, Stack } from "native-base";
import { useContext, useState, useEffect } from "react"; 
import { UserContext } from "../context/UserContext";
import { useAuth } from "../api/userAuth";
import Loader from '../components/Loader';
import { SafeAreaView, StyleSheet } from 'react-native';
import Boxe from "../components/Boxe";
import { RootTabParamList } from "../navigation/TabNavigator";
import { BottomTabScreenProps } from "@react-navigation/bottom-tabs";

interface Stat {
    count: number,
    average_age: number,
    max_age: number,
    min_age: number,
    count_men: number,
    count_girl: number,
}

type Props = BottomTabScreenProps<RootTabParamList, 'Home'>

const HomeScreen = ({ navigation }: Props) => {

    const user = useContext(UserContext);
    const [data, setData] = useState<Stat | null>(null);
    const { dashboard } = useAuth();
    const [loading, setLoading] = useState(true);
    //console.log('conect', user)

    useEffect(() => {
        dashboard().then((res) => {
            setData(res.data.data);
            setLoading(false);
        });
    }, []);

    const onscroll = (e: any) => {
        
        const currentOffset = e.nativeEvent.contentOffset.y;
        const dif = currentOffset - (e.offset || 0);  

        if (dif < 0) {

            navigation.setOptions({ 
                tabBarStyle: { display: 'none' }
            });
          } else {
            navigation.setOptions({
                tabBarStyle: { display: 'flex' }
            });
        }

        e.offset = currentOffset

    }

    return (
        <SafeAreaView style={styles.container}>
            
            { loading ?

                <Loader />:
                
                <ScrollView py={10}>
                    <Stack space={3}>
                        <HStack space={3} justifyContent="center">
                            <Boxe title="Nombre d'eleves" value={data?.count} />
                            <Boxe title="Moyenne d'age" value={data?.average_age} />
                        </HStack>
                        <HStack space={3} justifyContent="center">
                            <Boxe title="Nombre de garçons" value={data?.count_men} />
                            <Boxe title="Nombre de filles" value={data?.count_girl} />
                        </HStack>
                        <HStack space={3} justifyContent="center">
                            <Boxe title="Age maximal" value={data?.max_age} />
                            <Boxe title="Age minimal" value={data?.min_age} />
                        </HStack>
                    </Stack>
                </ScrollView>
            }
        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
});

export default HomeScreen;