import {  Box, Heading, FlatList } from "native-base";
import { useState, useEffect } from "react"; 
import { useAuth } from "../api/userAuth";
import { SafeAreaView, StyleSheet } from 'react-native';
import StudentItem from "../components/Student";
import SkeletonLoader from "../components/SkeletonLoader";
import { Student } from '../models';
import { RootTabParamList } from "../navigation/TabNavigator";
import { BottomTabScreenProps } from "@react-navigation/bottom-tabs";

type Props = BottomTabScreenProps<RootTabParamList, 'Student'>

const StudentScreen = ({ navigation }: Props) => {

    const [loading, setLoading] = useState(true);
    const [students, setStudents] = useState<Student[]>([]);
    const { studentList, studentListPaginator } = useAuth();
    const [lastPage, setLastPage] = useState(0);
    const [from, setFrom] = useState(2);

    useEffect(() => { 
        function fetch() {
            studentList().then((res) => {
            setLastPage(res.data.last_page);
            setStudents(res.data.data);
            setLoading(false);
            });
        }
        fetch();
    }, []);

    const diplayStudentDetails = (student: Student) => { 
        navigation.navigate('StudentDetails', {
            student: student
        })
    }

    const loadMore = () => {
    
        if(from <= lastPage) {

            setLoading(true);

            studentListPaginator(from).then((res) => {
              
                const reponseArray = res.data.data;
                let myArray: Student[] = [];

                for(const key in reponseArray) {
                    const value = reponseArray[key];
                    const objectResponse = {
                        id: value.id,
                        name: value.name,
                        surname: value.surname,
                        matricule: value.matricule,
                        sexe: value.sexe,
                        fName: value.father_name,
                        mName: value.mother_name,
                        fPhone: value.fphone,
                        mPhone: value.mphone,
                        quarter: value.quarter,
                        allergy: value.allergy,
                        bornAt: value.born_at,
                        bornPlace: value.born_place,
                    }
                    myArray.push(objectResponse);
                }
            
                setStudents([...students, ...myArray]);
                setLoading(false);
            });

            setFrom(from + 1)
        }
    }

    return (
        <SafeAreaView style={styles.container}>

            { loading ? 

                <SkeletonLoader />:
            
                <Box>
                    <Heading fontSize="xl" p="4" pb="3">
                        Liste des éléves
                    </Heading>
                    <FlatList 
                        data={students} 
                        renderItem={({
                        item
                        }) => <StudentItem student={item} diplayStudentDetails={diplayStudentDetails} /> } 
                        keyExtractor={item => item.id} 
                        onEndReached={loadMore}
                        onEndReachedThreshold={0.9}
                    />
                </Box>

            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
});

export default StudentScreen;