import { Center, FormControl, VStack, Avatar, Box, ScrollView, Heading } from "native-base";
import { SafeAreaView, StyleSheet } from 'react-native';
import HeaderNavigation from "../components/HeaderNavigation";
import InputField from "../components/InputField";
import { useContext, useState } from "react";
import { UserContext } from "../context/UserContext";
import MyButton from "../components/MyButton";
import { RootTabParamList } from "../navigation/TabNavigator";
import { MaterialBottomTabScreenProps } from "@react-navigation/material-bottom-tabs";
import { useAuth } from "../api/userAuth";
import AsyncStorage from "@react-native-async-storage/async-storage";

type Props = MaterialBottomTabScreenProps<RootTabParamList, 'Profile'>

const ProfileScreen = ({ navigation }: Props) => {

    const { userData, setUserData } = useContext(UserContext);
    const {  logOut, profile } = useAuth();

    const [name, setName] = useState(userData ? userData.name : '');
    const [surname, setSurname] = useState(userData ? userData.surname : '');
    const [phone, setPhone] = useState(userData ? userData.phone : '');
    const [loading, setLoading] = useState(false);

    const navigate = () => navigation.goBack(); 

    const save = () => {
        
        setLoading(true);

        profile(name, phone, surname).then(res => { 

            const user = {
                name: name,
                surname: surname,
                email: userData?.email ? userData?.email : '',
                phone: phone,
                rules: userData?.rules ? userData?.rules : []
            }

            setUserData(user);
            setLoading(false);

        })
    }

    const logout = () => {
        logOut().then((res) => {
            if(res.status == 204) {
                setUserData(null);
                AsyncStorage.multiRemove([
                    'token',
                    'name',
                    'surname',
                    'email',
                    'rules'
                ])
            }
        })
    }

    return (
        <SafeAreaView style={styles.container}>

            <HeaderNavigation title="Profile" navigate={navigate}  end={true} logout={logout} /> 

            <ScrollView>

                <Box px={'3'} py="8"  alignItems={'center'}>
                    
                    <VStack space={5} w={'full'}>

                        <Box alignItems={'center'}>
                            <Avatar size="lg">
                                { userData?.surname.charAt(0) && userData?.surname.charAt(0) &&  (userData?.surname.charAt(0)).toUpperCase() + (userData?.name.charAt(0)).toUpperCase()  }
                            </Avatar>
                            <Heading color="coolGray.600" fontWeight="medium" size="xs">
                                { userData?.email }
                            </Heading>
                        </Box> 
                        
                        <FormControl>
                            <FormControl.Label>Prénom</FormControl.Label>
                            <InputField type="text" value={surname}  onChangeText={(value: string) => setSurname(value)}/>
                        </FormControl>

                        <FormControl>
                            <FormControl.Label>Nom</FormControl.Label>
                            <InputField type="text" value={name}  onChangeText={(value: string) => setName(value)}/>
                        </FormControl>
                        

                        <FormControl>
                            <FormControl.Label>Téléphone</FormControl.Label>
                            <InputField type="text" value={phone}  onChangeText={(value: string) => setPhone(value)} />
                        </FormControl>

                        <MyButton mt={2} isDisabled={loading} isLoading={loading} label={"Sauvegarder"} isLoadingText={"Sauvegarde ..."} onPress={save} />

                    </VStack>

                </Box>

            </ScrollView>

        </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
  });

export default ProfileScreen;