import { 
    Center, 
    Box, 
    Heading, 
    VStack, 
    FormControl, 
    Link,
    Flex,
    Pressable,
    Icon,
    WarningOutlineIcon
} from "native-base";
import InputField from '../components/InputField';
import MyButton from '../components/MyButton';
import { SafeAreaView, StyleSheet } from 'react-native';
import { Image } from 'native-base';
import { useAuth } from "../api/userAuth";
import { useState } from 'react';
//import * as Device from 'expo-device'; 
import AsyncStorage from '@react-native-async-storage/async-storage';
import { UserContext } from '../context/UserContext';
import { useContext, useEffect } from "react"; 
import { MaterialIcons } from '@expo/vector-icons';
import HeaderNavigation from "../components/HeaderNavigation";
import Error from "../components/Error";
import type { StackScreenProps  } from '@react-navigation/stack';
import { RootStackParamList } from "../navigation/StackNavigator";

const logo = './../assets/logo.png';

type Props = StackScreenProps<RootStackParamList, 'Login'>;

const LoginScreen = ({ navigation }: Props) => {
    
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [errors, setErrors] = useState(null);
    const [show, setShow] = useState(false);
    const [displaySplash, setDisplaySplash] = useState(true);
    const [isEmailInvalid, setIsEmailInvalid] = useState(false);
    const [isPasswordInvalid, setIsPasswordInvalid] = useState(false);
   
    const { signIn } = useAuth();
    const { setUserData, setToken } = useContext(UserContext);

    const connexion = () => {

        if(!email) {
          setIsEmailInvalid(true);
          return;
        }

        if(!password) {
          setIsPasswordInvalid(true);
          return;
        }

        setLoading(true);

        signIn(email, password, 'name').then((res) => {
            setUserData(res.data.user);
            setToken(res.data.token);
            setLoading(false);
            AsyncStorage.setItem('token', res.data.token);
            AsyncStorage.setItem('name', res.data.user.name);
            AsyncStorage.setItem('surname', res.data.user.surname);
            AsyncStorage.setItem('phone', res.data.user.phone);
            AsyncStorage.setItem('email', res.data.user.email);
            AsyncStorage.setItem('rules', JSON.stringify(res.data.user.rules));
        }).catch(err => {
          setLoading(false);
          setErrors(err.response.data);
        })
        
    }

    useEffect(() => {
      const timer = setTimeout(() => {
        setDisplaySplash(false);
      }, 2000);
      return () => clearTimeout(timer);
    }, [])

    return <>
        {!displaySplash ?
          <SafeAreaView style={styles.container}>

            <HeaderNavigation title="Connexion" /> 

            <Flex px={'3'} py={'5'} height={"80%"} justifyContent={'space-between'}>

              <Box alignItems={'center'} mb="8">
                <Image 
                  source={require(logo)}
                  alt="logo"
                  style={{width: 160, height: 100}}
                />
              </Box>

              <Heading size="lg" fontWeight="600" color="coolGray.800">
                Bienvenue
              </Heading>

              <Heading color="coolGray.600" fontWeight="medium" size="xs">
                Connectez vous!
              </Heading> 

              {errors  && (( <Box mt={'3'}><Error errors={errors}></Error></Box>))}
    
              <VStack space={2} mt="2">
                
                <FormControl isInvalid={isEmailInvalid}>
                  <FormControl.Label>Email</FormControl.Label>
                  <InputField type="text" onChangeText={(value: string) => setEmail(value)} isInvalid={isEmailInvalid} />
                  <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                    Renseigner le mail
                  </FormControl.ErrorMessage>
                </FormControl>
                
                <FormControl isInvalid={isPasswordInvalid}>
                  <FormControl.Label>Password</FormControl.Label>
                  <InputField 
                    type={show ? "text" : "password"} 
                    onChangeText={(value: string) => setPassword(value)}
                    InputRightElement={<Pressable onPress={() => setShow(!show)}>
                                <Icon as={<MaterialIcons name={show ? "visibility" : "visibility-off"} />} size={5} mr="2" color="muted.400" />
                    </Pressable>}
                    isInvalid={isPasswordInvalid}
                  />
                  <FormControl.ErrorMessage leftIcon={<WarningOutlineIcon size="xs" />}>
                    Renseigner le mot de passe
                  </FormControl.ErrorMessage>
                  <Link _text={{
                    fontSize: "xs",
                    fontWeight: "500",
                    color: "main.50"
                    }} 
                    alignSelf="flex-end"
                    mt="1"
                    onPress={() => navigation.navigate('Forget')}
                    >
                    Mot de passe oublié?
                  </Link>
                </FormControl>
                
                <MyButton mt={2} isDisabled={loading} isLoading={loading} label={"Connexion"} isLoadingText={"Connexion ..."} onPress={connexion} />

              </VStack>
            </Flex>
      </SafeAreaView>:

      <SafeAreaView style={[styles.container, {justifyContent: 'center', alignItems: 'center'}]}>
              <Image 
                source={require(logo)}
                alt="logo"
                style={{width: 220, height: 150}}
              />
      </SafeAreaView>

      }
  </>;
};

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: 'white'
  },
});


export default LoginScreen;