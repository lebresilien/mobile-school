import React from 'react';
import { render } from '@testing-library/react-native';
import renderer from 'react-test-renderer';
import HomeScreen from '../screens/HomeScreen';
import { it } from '@jest/globals';

/* jest.mock('@react-native-async-storage/async-storage', () =>
  require('@react-native-async-storage/async-storage/jest/async-storage-mock')
);

jest.mock("react", () => {
    const originReact = jest.requireActual("react");
    const mUseRef = jest.fn();
    return {
      ...originReact,
      useRef: mUseRef,
    };
});
 */
test('renders correctly', () => {
    const tree = renderer.create(<HomeScreen />).toJSON();
    expect(tree).toMatchSnapshot();
  });