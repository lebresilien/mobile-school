import { Input } from 'native-base';

type Props = {
    type: 'text' | 'password' | undefined,
    placeholder?: string,
    onChangeText: (value: string) => void,
    isInvalid?: boolean,
    InputRightElement?: any,
    value?: string
}

const InputField = ({ type, placeholder = '', onChangeText, isInvalid, InputRightElement, value }: Props) => (
    <Input
        type={type}
        placeholder={placeholder ? placeholder : ''}
        onChangeText={onChangeText}
        isInvalid={isInvalid}
        InputRightElement={InputRightElement}
        value={value}
    />
)

export default InputField