import {Text, Box, VStack, Center, Flex} from 'native-base';

type ErrorProps = {
    errors: {
        message: string,
        errors: any
    } | null
}

const Error = ({ errors }: ErrorProps) => (
    <Center>
        <VStack>
            <Text color={'red.500'}>{  errors?.message }</Text>
        </VStack>
    </Center>
)


export default Error;