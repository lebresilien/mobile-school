import { Box, HStack, Avatar, VStack, Spacer, Text, Pressable } from 'native-base';
import React from 'react';
import { AntDesign } from '@expo/vector-icons';
import { Student } from '../models';

type Props = {
    student: Student,
    diplayStudentDetails: (student: Student) => void
}

const StudentItem = ({ student, diplayStudentDetails }: Props) => (

    <Box borderBottomWidth="1" borderColor="muted.800" pl={["0", "4"]} pr={["0", "5"]} py="2">
        <Pressable onPress={() => diplayStudentDetails(student)}>
            <HStack space={[2, 3]} justifyContent="space-between">
                <Avatar size="70px">{ (student.name.charAt(0)).toUpperCase() + (student.surname.charAt(0)).toUpperCase() }</Avatar>
                <VStack>
                    <Text color="coolGray.800" bold>
                        { student.name + ' ' + student.surname }
                    </Text>
                    <Text color="coolGray.600">
                        { student.matricule }
                    </Text>
                </VStack>
                <Spacer />
                <AntDesign name="arrowright" size={24} color="black" />
            </HStack>
        </Pressable>
    </Box>
)


export default StudentItem;