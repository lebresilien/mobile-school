import React from 'react';
import { Skeleton, HStack, Stack } from 'native-base';

const Loader = () => (
    <Stack space={3}>
        <HStack space={3}  justifyContent="center">
            <Skeleton h="40" w="40" />
            <Skeleton h="40" w="40" />
        </HStack>
        <HStack space={3}  justifyContent="center">
            <Skeleton h="40" w="40" />
            <Skeleton h="40" w="40" />
        </HStack>
        <HStack space={3}  justifyContent="center">
            <Skeleton h="40" w="40" />
            <Skeleton h="40" w="40" />
        </HStack>
    </Stack>
);

export default Loader;
