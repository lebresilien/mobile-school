import { Flex, Text, Box, Menu, Pressable } from 'native-base';
import { MaterialCommunityIcons } from '@expo/vector-icons';

type Props = {
    title: string,
    navigate?(): void,
    end?: boolean,
    logout?(): void,
}

const HeaderNavigation = ({ title, navigate, end, logout }: Props) => (
    <>
        { navigate ? 
            <Box>
                {!end ? 
                    
                    <Flex bg={"main.50"} py={"4"} width={"full"} direction='row' justify='start'>
                        <MaterialCommunityIcons name="arrow-left-thin" size={26} color={'white'} onPress={navigate} />
                        {' '}{' '}
                        <Text color={'white'}>{ title }</Text>
                    </Flex>
                    :
                    <Flex bg={"main.50"} px={'2'} py={"4"} width={"full"} direction='row' justify='space-between'>
                        
                        <Flex direction='row'>
                            <MaterialCommunityIcons name="arrow-left-thin" size={26} color={'white'} onPress={navigate} />
                                {' '}{' '}
                            <Text color={'white'}>{ title }</Text>
                        </Flex>

                        <Flex>
                            <Menu shadow={2} w="190" trigger={triggerProps => {
                            return <Pressable accessibilityLabel="More options menu" {...triggerProps}>
                                        <MaterialCommunityIcons name="logout" size={24} color="white" />
                                    </Pressable>;
                            }}>
                                <Menu.Item color={'white'} onPress={logout} >Déconnexion</Menu.Item>
                                
                            </Menu>
                        </Flex>

                    </Flex>
                }
            </Box>
            :
            <Flex bg={"main.50"} py={"4"} width={"full"} direction='row' justify='start'>
                <Text color={'white'}>{ title }</Text>
            </Flex>
        }
    </>
);

export default HeaderNavigation;