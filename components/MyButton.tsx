import { Button } from 'native-base';

type Props = {
    label: string,
    onPress: () => void,
    mt?: number,
    isDisabled: boolean,
    isLoading: boolean,
    isLoadingText: string,
    w?: string
}

const MyButton = ({ onPress, label, w, mt, isDisabled, isLoading, isLoadingText }: Props) => (
    <Button 
        onPress={onPress}
        mt={mt}
        isDisabled={isDisabled}
        isLoading={isLoading}
        isLoadingText={isLoadingText}
        w={w}
    >
        {label}
    </Button>
)

export default MyButton;