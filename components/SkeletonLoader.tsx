import React from 'react';
import { Skeleton, HStack, Stack, VStack } from 'native-base';

const SkeletonLoader = () => (

    <>
        <HStack alignItems="center" w="100%" maxW="400" space={4} rounded="md" p="4">
            <Skeleton size="20" rounded="full" />
            <Skeleton.Text  />
        </HStack>

        <HStack alignItems="center" w="100%" maxW="400" space={4} rounded="md" p="4">
            <Skeleton size="20" rounded="full" />
            <Skeleton.Text  />
        </HStack>

        <HStack alignItems="center" w="100%" maxW="400" space={4} rounded="md" p="4">
            <Skeleton size="20" rounded="full" />
            <Skeleton.Text  />
        </HStack>

        <HStack alignItems="center" w="100%" maxW="400" space={4} rounded="md" p="4">
            <Skeleton size="20" rounded="full" />
            <Skeleton.Text  />
        </HStack>

        <HStack alignItems="center" w="100%" maxW="400" space={4} rounded="md" p="4">
            <Skeleton size="20" rounded="full" />
            <Skeleton.Text  />
        </HStack>
    </>
);

export default SkeletonLoader;