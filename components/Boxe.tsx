import { Box, Center, Heading, Flex } from "native-base"

type Props = {
    title: string,
    value: number | undefined
}

const Boxe = ({ title, value }: Props) => (
    <Box h="40" w="40" background={'white'} borderWidth={'1'} overflow="hidden" borderColor={'coolGray.200'} rounded={'lg'} alignContent={'center'}>
       <Center w={'full'} h={'full'}>
            <Flex w={'full'} h={'full'} direction="column" justifyContent={'space-between'} py={'10'} align="center">
                <Heading color="coolGray.600" fontWeight="medium" size="xs">{ title }</Heading>
                <Heading color="coolGray.600" fontWeight="medium" size="xs">{ value }</Heading>
            </Flex>
        </Center> 
    </Box>
)

export default Boxe;