import 'react-native-gesture-handler';
import { StyleSheet, Text, View } from 'react-native';
import { NativeBaseProvider, extendTheme  } from "native-base";
import MainNavigator from './navigation/MainNavigator';
import NetInfo from '@react-native-community/netinfo';
import React, { useState, useEffect } from 'react';
import { User } from "./models";
import { UserContext } from "./context/UserContext";
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function App() {

  const theme = extendTheme({
      colors: {
        main: {
          50: '#083d86'
        }
      },
      components: {
        Button: {
          baseStyle: {
            rounded: 'md',
          },
          defaultProps: {
            bg: 'main.50',
          }
        },
        Input: {
          defaultProps: {
            bg: 'coolGray.100',
            borderColor: 'gray.50',
            _hover: {
              bg: "coolGray.200",
              borderColor: 'gray.50',
            },
            _focus: {
              bg: "coolGray.200:alpha.70",
              borderColor: 'gray.50',
            }
          }
        }
      }
  });

  const [isConnected, setIsConnected] = useState<boolean | null>(false);
  const [userData, setUserData] = useState<User | null>(null);
  const [ token, setToken ] = useState<string | null>(null);

  const value = {
    userData,
    setUserData,
    token,
    setToken
  };
  

  useEffect(() => {
      NetInfo.fetch().then((state) => {
        setIsConnected(state.isConnected)
        console.log('conected valuue', isConnected)
      })
  })

  useEffect(() => {

    const Init = async () => {

      const name =  await AsyncStorage.getItem('name');
      const surname =  await AsyncStorage.getItem('surname');
      const email =  await AsyncStorage.getItem('email');
      const phone =  await AsyncStorage.getItem('phone');
      const rules =  await AsyncStorage.getItem('rules');
      const token =  await AsyncStorage.getItem('token');
     
      if(name && email && surname && rules && phone) {  
        
        const user = {
          name: name,
          surname: surname,
          email: email,
          phone: phone,
          rules: JSON.parse(rules)
        }

        setUserData(user);
        setToken(token);
      
      }

    };
  
    Init(); // run it, run it
  }, [])

  return (
      <NativeBaseProvider theme={theme}>
        <UserContext.Provider value={value}>
          <MainNavigator />
        </UserContext.Provider>
      </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
