import { NavigationContainer } from '@react-navigation/native';
import MyTab from './TabNavigator';
import MyStack from './StackNavigator';
import { UserContext } from '../context/UserContext';
import { useContext } from "react"; 


function MainNavigator() {

    const { userData } = useContext(UserContext);

    return (
        <NavigationContainer>
            { !userData ? <MyStack /> : <MyTab /> }
        </NavigationContainer>
    )
}

export default MainNavigator;