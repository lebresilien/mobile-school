import { MaterialCommunityIcons } from '@expo/vector-icons';
import HomeScreen from '../screens/HomeScreen';
import ProfileScreen from '../screens/ProfileScreen';
import StudentScreen from '../screens/StudentScreen';
import NoteScreen from '../screens/NoteScreen';
import CourseScreen from '../screens/CourseScreen';
import StudentDetailsScreen from '../screens/StudentDetailsScreen';
import { BottomTabNavigationEventMap, createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { TouchableOpacity } from 'react-native';
import { NavigationHelpers, ParamListBase, TabNavigationState } from '@react-navigation/native';
import { BottomTabDescriptorMap } from '@react-navigation/bottom-tabs/lib/typescript/src/types';
import { Box, Center, Flex, VStack, Text } from 'native-base';
import { Student } from '../models';

export type RootTabParamList = {
    Profile: undefined;
    Home: undefined;
    Student: undefined,
    StudentDetails: { student: Student } | undefined,
    Note: undefined,
    Course: undefined
};

interface TabBarProps {
    state: TabNavigationState<ParamListBase>;
    descriptors: BottomTabDescriptorMap;
    navigation: NavigationHelpers<ParamListBase, BottomTabNavigationEventMap>;
}
  
const Tab = createBottomTabNavigator<RootTabParamList>();

function MyTabBar({ state, descriptors, navigation }: TabBarProps) {
    
    const focusedOptions = descriptors[state.routes[state.index].key].options;
    if (focusedOptions?.tabBarStyle?.display === "none") {
      return null;
    }
    
    return (
      
        <Flex bg={'main.50'} height={50} direction='row' px={10} align='center'>
        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;
  
         if(label != 'StudentDetails' && label != 'Course') {
          const isFocused = state.index === index;
  
          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });
  
            if (!isFocused && !event.defaultPrevented) {
              // The `merge: true` option makes sure that the params inside the tab screen are preserved
              navigation.navigate({ name: route.name, merge: true });
            }
          };
  
          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };
  
          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityState={isFocused ? { selected: true } : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{ flex: 1 }}
            >
                <VStack>
                    <Center>
                        {label == "Home" && ((
                            <MaterialCommunityIcons name="home" color={'#eee'} size={20} />
                        ))}
                        {label == "Student" && ((
                            <MaterialCommunityIcons name="school" color={'#eee'} size={20} />
                        ))}
                        {label == "Profile" && ((
                            <MaterialCommunityIcons name="account" color={'#eee'} size={20} />
                        ))}
                         {label == "Note" && ((
                            <MaterialCommunityIcons name="note-plus-outline" size={0} color="#eee" />
                        ))}
                        <Text fontSize="xs" style={{ color: isFocused ? '#673ab7' : '#eee' }}>
                            {label}
                        </Text>
                    </Center>
              </VStack>
            </TouchableOpacity>
          );
        }})}
        </Flex>
     
    );
  }
function MyTab() {
  return (
    <Tab.Navigator screenOptions={{headerShown:false, tabBarStyle: { position: 'absolute' }}} tabBar={props => <MyTabBar {...props} />}>
        <Tab.Screen 
            name="Home"
            component={HomeScreen} 
        />
        <Tab.Screen 
            name="Student" 
            component={StudentScreen}
        />
        <Tab.Screen 
            name="Profile" 
            component={ProfileScreen}
        />
        <Tab.Screen 
            name="StudentDetails" 
            component={StudentDetailsScreen}
        />
         <Tab.Screen 
            name="Note" 
            component={NoteScreen}
        />
         <Tab.Screen 
            name="Course" 
            component={CourseScreen}
        />
    </Tab.Navigator>
  );
}

export default MyTab;