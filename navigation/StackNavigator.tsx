import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../screens/LoginScreen';
import ForgetScreen from '../screens/ForgetScreen';
import ResetScreen from '../screens/ResetScreen';

export type RootStackParamList = {
  Login: undefined;
  Forget: undefined;
  Reset:  undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

function MyStack() {
    return (
        <Stack.Navigator
          screenOptions={{
            headerShown: false
          }}
        >
          <Stack.Screen
            name="Login" 
            component={LoginScreen} 
          />
          <Stack.Screen 
            name="Forget" 
            component={ForgetScreen} 
          />
          <Stack.Screen 
            name="Reset" 
            component={ResetScreen} 
          />
        </Stack.Navigator>
    );
}

export default MyStack;